The aim of this project is to design a visual transformer system.
To verify the system, we visualize the three transformer models to improve the interpretability of the model.
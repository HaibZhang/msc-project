from PIL import Image
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import torch
import numpy as np
import cv2
from imagenet_label import CLS2IDX
from explain.util.configs import CONFIGS
from explain.swin.swin_explain import SWIN
from explanation_generator import LRP


config = CONFIGS['Swin-B_16']
print("config",config)
normalize = transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])
transform = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    normalize,
])

# create heatmap from mask on image
def show_cam_on_image(img, mask):
    heatmap = cv2.applyColorMap(np.uint8(255 * mask), cv2.COLORMAP_JET)
    heatmap = np.float32(heatmap) / 255
    cam = heatmap + np.float32(img)
    cam = cam / np.max(cam)
    return cam

# initialize ViT pretrained
model = SWIN(config, 224, num_classes=1000)
# model.load_from(torch.load("checkpoint\swin_base_patch4_window7_224.pth"))
model.load_from(torch.load("D:/20-21semester2/MscProject/tra2/transformer/checkpoint/checkpoint/swin_base_patch4_window7_224.pth", map_location=torch.device('cpu')))
# model.to("cuda")
model.eval()
attribution_generator = LRP(model)

def generate_visualization(original_image, class_index=None):
    # transformer_attribution = attribution_generator.generate_LRP(original_image.unsqueeze(0).cuda(), index=class_index).detach()
    transformer_attribution = attribution_generator.generate_LRP(original_image.unsqueeze(0),
                                                                 index=class_index).detach()
    origin_w = original_image.size(-1)
    scale_factor = int(origin_w/transformer_attribution.size(-1))
    transformer_attribution = torch.nn.functional.interpolate(transformer_attribution, scale_factor=scale_factor, mode='bilinear')
    # transformer_attribution = transformer_attribution.reshape(224, 224).cuda().data.cpu().numpy()
    transformer_attribution = transformer_attribution.reshape(224, 224).data.cpu().numpy()
    transformer_attribution = (transformer_attribution - transformer_attribution.min()) / (transformer_attribution.max() - transformer_attribution.min())
    image_transformer_attribution = original_image.permute(1, 2, 0).data.cpu().numpy()
    image_transformer_attribution = (image_transformer_attribution - image_transformer_attribution.min()) / (image_transformer_attribution.max() - image_transformer_attribution.min())
    vis = show_cam_on_image(image_transformer_attribution, transformer_attribution)
    vis =  np.uint8(255 * vis)
    vis = cv2.cvtColor(np.array(vis), cv2.COLOR_RGB2BGR)
    return vis

def print_top_classes(predictions, **kwargs):    
    # Print Top-5 predictions
    prob = torch.softmax(predictions, dim=1)
    class_indices = predictions.data.topk(5, dim=1)[1][0].tolist()
    max_str_len = 0
    class_names = []
    for cls_idx in class_indices:
        class_names.append(CLS2IDX[cls_idx])
        if len(CLS2IDX[cls_idx]) > max_str_len:
            max_str_len = len(CLS2IDX[cls_idx])
    
    print('Top 5 classes:')
    for cls_idx in class_indices:
        output_string = '\t{} : {}'.format(cls_idx, CLS2IDX[cls_idx])
        output_string += ' ' * (max_str_len - len(CLS2IDX[cls_idx])) + '\t\t'
        output_string += 'value = {:.3f}\t prob = {:.1f}%'.format(predictions[0, cls_idx], 100 * prob[0, cls_idx])
        print(output_string)
    return class_indices

# Image: catdog.png
image = Image.open('samples\catdog.png')
dog_cat_image = transform(image)

fig, axs = plt.subplots(1, 4)
axs[0].imshow(image);
axs[0].axis('off');

# output = model(dog_cat_image.unsqueeze(0).cuda())
output = model(dog_cat_image.unsqueeze(0))
class_indices = print_top_classes(output)

# the predicted class
first_class_image = generate_visualization(dog_cat_image,class_index=class_indices[0])

# generate visualization for second predict class images
second_class_image = generate_visualization(dog_cat_image, class_index=class_indices[1])

# generate visualization for third predict class images
third_class_image = generate_visualization(dog_cat_image, class_index=class_indices[2])


axs[1].imshow(first_class_image);
axs[1].axis('off');
axs[2].imshow(second_class_image);
axs[2].axis('off');
axs[3].imshow(third_class_image);
axs[3].axis('off');


plt.show()



import torch
import torch.nn as nn
import torch.nn.functional as F
from functools import partial
from explain.util.weight_init import trunc_normal_,pair
from explain.util.explain_layers import *
import math


def compute_rollout_attention(all_layer_matrices, start_layer=0):
    # adding residual consideration
    num_tokens = all_layer_matrices[0].shape[1]
    batch_size = all_layer_matrices[0].shape[0]
    eye = torch.eye(num_tokens).expand(batch_size, num_tokens, num_tokens).to(all_layer_matrices[0].device)
    all_layer_matrices = [all_layer_matrices[i] + eye for i in range(len(all_layer_matrices))]
    # all_layer_matrices = [all_layer_matrices[i] / all_layer_matrices[i].sum(dim=-1, keepdim=True)
    #                       for i in range(len(all_layer_matrices))]
    joint_attention = all_layer_matrices[start_layer]
    for i in range(start_layer+1, len(all_layer_matrices)):
        joint_attention = all_layer_matrices[i].bmm(joint_attention)
    return joint_attention



class DWConv(nn.Module):
    def __init__(self, dim=768):
        super(DWConv, self).__init__()
        self.padding = 1
        self.kernel = 3
        self.stride = 1
        self.dwconv = Conv2d(dim, dim, self.kernel, self.stride, self.padding, bias=True, groups=dim)

    def forward(self, x, H, W):
        self.H = H
        self.W = W 
        B, self.N, C = x.shape
        x = x.transpose(1, 2).view(B, C, H, W)
        x = self.dwconv(x)
        x = x.flatten(2).transpose(1, 2)

        return x
    
    def relprop(self, cam, **kwargs):
        B,_,C = cam.shape
        cam = cam.transpose(1,2)
        output_H,output_W = (self.H+self.padding*2-self.kernel+1) // self.stride, (self.W+self.padding*2-self.kernel+1) // self.stride
        cam = cam.view(B,C,output_H,output_W)

        cam = self.dwconv.relprop(cam,**kwargs)
        cam = cam.view(B,C,-1)
        cam = cam.transpose(1,2)
        return cam


class Mlp(nn.Module):
    def __init__(self, in_features, hidden_features=None, out_features=None, act_layer=GELU, drop=0., linear=False):
        super().__init__()
        out_features = out_features or in_features
        hidden_features = hidden_features or in_features
        self.fc1 = Linear(in_features, hidden_features)
        self.dwconv = DWConv(hidden_features)
        self.act = act_layer()
        self.fc2 = Linear(hidden_features, out_features)
        self.drop = Dropout(drop)
        self.linear = linear
        if self.linear:
            self.relu = ReLU(inplace=True)
        self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, nn.Linear):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, nn.Linear) and m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif isinstance(m, nn.LayerNorm):
            nn.init.constant_(m.bias, 0)
            nn.init.constant_(m.weight, 1.0)
        elif isinstance(m, nn.Conv2d):
            fan_out = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
            fan_out //= m.groups
            m.weight.data.normal_(0, math.sqrt(2.0 / fan_out))
            if m.bias is not None:
                m.bias.data.zero_()


    def forward(self, x, H, W):
        x = self.fc1(x)
        if self.linear:
            x = self.relu(x)
        x = self.dwconv(x, H, W)
        x = self.act(x)
        x = self.drop(x)
        x = self.fc2(x)
        x = self.drop(x)
        return x


    def relprop(self, cam, **kwargs):
        cam = self.drop.relprop(cam,**kwargs)
        cam = self.fc2.relprop(cam,**kwargs)
        cam = self.drop.relprop(cam,**kwargs)
        cam = self.act.relprop(cam,**kwargs)
        cam = self.dwconv.relprop(cam,**kwargs)
        if self.linear:
            cam = self.relu.relprop(cam,**kwargs)
        cam = self.fc1.relprop(cam,**kwargs)
        return cam



class Attention(nn.Module):
    def __init__(self, dim, num_heads=8, qkv_bias=False, qk_scale=None, attn_drop=0., proj_drop=0., sr_ratio=1, linear=False):
        super().__init__()
        assert dim % num_heads == 0, f"dim {dim} should be divided by num_heads {num_heads}."

        self.dim = dim
        self.num_heads = num_heads
        head_dim = dim // num_heads
        self.scale = qk_scale or head_dim ** -0.5

        self.q = Linear(dim, dim, bias=qkv_bias)
        self.kv = Linear(dim, dim * 2, bias=qkv_bias)
        self.attn_drop = Dropout(attn_drop)
        self.proj = Linear(dim, dim)
        self.proj_drop = Dropout(proj_drop)
        self.matmul1 = MatMul()
        self.matmul2 = MatMul()
        self.softmax = Softmax(dim=-1)
        self.clone = Clone()

        self.linear = linear
        self.sr_ratio = sr_ratio
        if not linear:
            if sr_ratio > 1:
                self.sr = Conv2d(dim, dim, kernel_size=sr_ratio, stride=sr_ratio)
                self.norm = LayerNorm(dim)
        else:
            self.pool_size = 7
            self.pool = AdaptiveAvgPool2d(self.pool_size)
            self.sr = Conv2d(dim, dim, kernel_size=1, stride=1)
            self.norm = LayerNorm(dim)
            self.act = GELU()
        self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, nn.Linear):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, nn.Linear) and m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif isinstance(m, nn.LayerNorm):
            nn.init.constant_(m.bias, 0)
            nn.init.constant_(m.weight, 1.0)
        elif isinstance(m, nn.Conv2d):
            fan_out = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
            fan_out //= m.groups
            m.weight.data.normal_(0, math.sqrt(2.0 / fan_out))
            if m.bias is not None:
                m.bias.data.zero_()

    def get_attn(self):
        return self.attn

    def save_attn(self, attn):
        self.attn = attn

    def save_attn_cam(self, cam):
        self.attn_cam = cam

    def get_attn_cam(self):
        return self.attn_cam

    def get_v(self):
        return self.v

    def save_v(self, v):
        self.v = v

    def save_v_cam(self, cam):
        self.v_cam = cam

    def get_v_cam(self):
        return self.v_cam

    def save_attn_gradients(self, attn_gradients):
        self.attn_gradients = attn_gradients

    def get_attn_gradients(self):
        return self.attn_gradients

    def forward(self, x, H, W):
        self.H = H
        self.W = W 
        B, N, C = x.shape
        x1,x2 = self.clone(x,2)
        q = self.q(x1).reshape(B, N, self.num_heads, C // self.num_heads).permute(0, 2, 1, 3)

        if not self.linear:
            if self.sr_ratio > 1:
                x_ = x2.permute(0, 2, 1).reshape(B, C, H, W)
                x_ = self.sr(x_).reshape(B, C, -1).permute(0, 2, 1)
                x_ = self.norm(x_)
                kv = self.kv(x_).reshape(B, -1, 2, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)
            else:
                kv = self.kv(x2).reshape(B, -1, 2, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)
        else:
            x_ = x2.permute(0, 2, 1).reshape(B, C, H, W)
            x_ = self.sr(self.pool(x_)).reshape(B, C, -1).permute(0, 2, 1)
            x_ = self.norm(x_)
            x_ = self.act(x_)
            kv = self.kv(x_).reshape(B, -1, 2, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)
        k, v = kv[0], kv[1]

        # attn = (q @ k.transpose(-2, -1)) * self.scale
        attn = self.matmul1((q, k.transpose(-2, -1))) * self.scale
        # attn = attn.softmax(dim=-1)
        attn = self.softmax(attn)
        attn = self.attn_drop(attn)
        
        self.save_attn(attn)
        attn.register_hook(self.save_attn_gradients)

        # x = (attn @ v).transpose(1, 2).reshape(B, N, C)
        x = self.matmul2((attn, v)).transpose(1, 2).reshape(B, N, C)
        x = self.proj(x)
        x = self.proj_drop(x)

        return x

    def relprop(self, cam, **kwargs):
        B, N, C = cam.shape
        cam = self.proj_drop.relprop(cam,**kwargs)
        cam = self.proj.relprop(cam,**kwargs)
        new_cam_shape = cam.size()[:-1] + (self.num_heads,-1)
        cam = cam.view(*new_cam_shape)
        cam = cam.transpose(1,2)
        cam,cam_v = self.matmul2.relprop(cam,**kwargs)

        cam /= 2
        cam_v /= 2

        self.save_attn_cam(cam)
        self.save_v_cam(cam_v)

        cam = self.attn_drop.relprop(cam,**kwargs)
        cam = self.softmax.relprop(cam,**kwargs)
        cam_q,cam_k = self.matmul1.relprop(cam,**kwargs)
        cam_k = cam_k.transpose(-1,-2)
        cam_k = cam_k.unsqueeze(0)
        cam_v = cam_v.unsqueeze(0)

        cam_kv = torch.cat([cam_k,cam_v],dim=0)
        cam_kv = cam_kv.permute(1,3,0,2, 4)
        cam_kv = cam_kv.reshape(B,-1,2*C)

        if not self.linear:
            if self.sr_ratio > 1:
                cam_kv = self.kv.relprop(cam_kv,**kwargs)
                cam_kv = self.norm.relprop(cam_kv,**kwargs)
                cam_kv = cam_kv.permute(0, 2, 1)
                cam_kv = cam_kv.reshape(B, C,self.H//self.sr_ratio,self.W//self.sr_ratio)
                cam_kv = self.sr.relprop(cam_kv,**kwargs)
                cam_kv = cam_kv.reshape(B, C,-1)
                cam_kv = cam_kv.permute(0, 2, 1)
            else:
                cam_kv = self.kv.relprop(cam_kv,**kwargs)
        else:
            cam_kv = self.kv.relprop(cam_kv,**kwargs)
            cam_kv = self.act.relprop(cam_kv,**kwargs)
            cam_kv = self.norm.relprop(cam_kv,**kwargs)
            cam_kv = cam_kv.permute(0, 2, 1)
            cam_kv = cam_kv.reshape(B, C,self.pool_size,self.pool_size)
            cam_kv = self.sr.relprop(cam_kv,**kwargs)
            cam_kv = self.pool.relprop(cam_kv,**kwargs)
            cam_kv = cam_kv.reshape(B, C,-1)
            cam_kv = cam_kv.permute(0, 2, 1)

        cam_q = cam_q.permute(0, 2, 1, 3)
        cam_q = cam_q.reshape(B,N,C)
        cam_q = self.q.relprop(cam_q,**kwargs)
        
        cam = self.clone.relprop((cam_q,cam_kv),**kwargs)
        return cam


class Block(nn.Module):

    def __init__(self, dim, num_heads, mlp_ratio=4., qkv_bias=False, qk_scale=None, drop=0., attn_drop=0.,
                 drop_path=0., act_layer=GELU, norm_layer=LayerNorm, sr_ratio=1, linear=False):
        super().__init__()
        self.norm1 = norm_layer(dim)
        self.attn = Attention(
            dim,
            num_heads=num_heads, qkv_bias=qkv_bias, qk_scale=qk_scale,
            attn_drop=attn_drop, proj_drop=drop, sr_ratio=sr_ratio, linear=linear)
        # NOTE: drop path for stochastic depth, we shall see if this is better than dropout here
        self.drop_path1 = DropPath(drop_path) if drop_path > 0. else Identity()
        self.drop_path2 = DropPath(drop_path) if drop_path > 0. else Identity()

        self.norm2 = norm_layer(dim)
        mlp_hidden_dim = int(dim * mlp_ratio)
        self.mlp = Mlp(in_features=dim, hidden_features=mlp_hidden_dim, act_layer=act_layer, drop=drop, linear=linear)
        self.add1 = Add()
        self.add2 = Add()
        self.clone1 = Clone()
        self.clone2 = Clone()

        self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, nn.Linear):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, nn.Linear) and m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif isinstance(m, nn.LayerNorm):
            nn.init.constant_(m.bias, 0)
            nn.init.constant_(m.weight, 1.0)
        elif isinstance(m, nn.Conv2d):
            fan_out = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
            fan_out //= m.groups
            m.weight.data.normal_(0, math.sqrt(2.0 / fan_out))
            if m.bias is not None:
                m.bias.data.zero_()

    def forward(self, x, H, W):
        x1,x2 = self.clone1(x,2)
        x2 = self.norm1(x2)
        x2 = self.attn(x2, H, W)
        x2 = self.drop_path1(x2)
        x = self.add1((x1, x2))

        x1,x2 = self.clone2(x,2)
        x2 = self.norm2(x2)
        x2 = self.mlp(x2, H, W)
        x2 = self.drop_path2(x2)
        x = self.add2((x1, x2))

        return x


    def relprop(self, cam, **kwargs):
        cam_1,cam_2 = self.add2.relprop(cam,**kwargs)
        cam_2 = self.drop_path2.relprop(cam_2,**kwargs)
        cam_2 = self.mlp.relprop(cam_2,**kwargs)
        cam_2 = self.norm2.relprop(cam_2,**kwargs)
        cam = self.clone2.relprop((cam_1,cam_2),**kwargs)
        
        cam_1,cam_2 = self.add1.relprop(cam,**kwargs)
        cam_2 = self.drop_path1.relprop(cam_2,**kwargs)
        cam_2 = self.attn.relprop(cam_2,**kwargs)
        cam_2 = self.norm1.relprop(cam_2,**kwargs)
        cam = self.clone1.relprop((cam_1,cam_2),**kwargs)
        return cam

class OverlapPatchEmbed(nn.Module):
    """ Image to Patch Embedding
    """

    def __init__(self, img_size=224, patch_size=7, stride=4, in_chans=3, embed_dim=768):
        super().__init__()
        img_size = pair(img_size)
        patch_size = pair(patch_size)

        self.img_size = img_size
        self.patch_size = patch_size
        self.stride = stride
        self.H, self.W = img_size[0] // patch_size[0], img_size[1] // patch_size[1]
        self.num_patches = self.H * self.W
        self.proj = Conv2d(in_chans, embed_dim, kernel_size=patch_size, stride=stride,
                              padding=(patch_size[0] // 2, patch_size[1] // 2))
        self.norm = LayerNorm(embed_dim)

        self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, nn.Linear):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, nn.Linear) and m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif isinstance(m, nn.LayerNorm):
            nn.init.constant_(m.bias, 0)
            nn.init.constant_(m.weight, 1.0)
        elif isinstance(m, nn.Conv2d):
            fan_out = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
            fan_out //= m.groups
            m.weight.data.normal_(0, math.sqrt(2.0 / fan_out))
            if m.bias is not None:
                m.bias.data.zero_()

    def forward(self, x):
        x = self.proj(x)
        B, C, H, W = x.shape
        x = x.flatten(2).transpose(1, 2)
        x = self.norm(x)

        return x, H, W

    def relprop(self, cam, **kwargs):
        B, N, C = cam.shape
        cam = self.norm.relprop(cam,**kwargs)
        cam = cam.transpose(1,2)
        output_H,output_W = (self.img_size[0]+self.patch_size[0]-self.patch_size[0]+1) // self.stride, (self.img_size[1]+self.patch_size[1]-self.patch_size[1]+1) // self.stride
        cam = cam.reshape(B, C,output_H,output_W)

        cam = self.proj.relprop(cam,**kwargs)
        return cam


class PVT(nn.Module):
    def __init__(self,config, img_size=224,  num_classes=1000, norm_layer=LayerNorm):
        super().__init__()
        self.num_classes = num_classes
        self.num_stages = config.num_stages
        self.patch_size = config.patch_size
        self.in_chans = config.in_chans
        self.embed_dims = config.embed_dims
        self.num_heads = config.num_heads
        self.mlp_ratios = config.mlp_ratios
        self.qkv_bias = config.qkv_bias
        self.qk_scale = config.qk_scale
        self.drop_rate = config.drop_rate
        self.attn_drop_rate = config.attn_drop_rate
        self.drop_path_rate = config.drop_path_rate
        self.depths = config.depths
        self.sr_ratios = config.sr_ratios
        self.linear = config.linear
        self.img_size = img_size
        self.mlp_head = config.mlp_head


        self.dpr = [x.item() for x in torch.linspace(0, self.drop_path_rate, sum(self.depths))]  # stochastic depth decay rule
        cur = 0
        self.input_resolutions = []

        for i in range(self.num_stages):
            patch_embed = OverlapPatchEmbed(img_size=self.img_size if i == 0 else self.img_size // (2 ** (i + 1)),
                                            patch_size=7 if i == 0 else 3,
                                            stride=4 if i == 0 else 2,
                                            in_chans=self.in_chans if i == 0 else self.embed_dims[i - 1],
                                            embed_dim=self.embed_dims[i])

            block = nn.ModuleList([Block(
                dim=self.embed_dims[i], num_heads=self.num_heads[i], mlp_ratio=self.mlp_ratios[i], qkv_bias=self.qkv_bias, qk_scale=self.qk_scale,
                drop=self.drop_rate, attn_drop=self.attn_drop_rate, drop_path=self.dpr[cur + j], norm_layer=norm_layer,
                sr_ratio=self.sr_ratios[i], linear=self.linear)
                for j in range(self.depths[i])])
            norm = norm_layer(self.embed_dims[i])
            cur += self.depths[i]

            setattr(self, f"patch_embed{i + 1}", patch_embed)
            setattr(self, f"block{i + 1}", block)
            setattr(self, f"norm{i + 1}", norm)

        # classification head
        self.head = Linear(self.embed_dims[3], num_classes)

        self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, nn.Linear):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, nn.Linear) and m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif isinstance(m, nn.LayerNorm):
            nn.init.constant_(m.bias, 0)
            nn.init.constant_(m.weight, 1.0)
        elif isinstance(m, nn.Conv2d):
            fan_out = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
            fan_out //= m.groups
            m.weight.data.normal_(0, math.sqrt(2.0 / fan_out))
            if m.bias is not None:
                m.bias.data.zero_()

    def freeze_patch_emb(self):
        self.patch_embed1.requires_grad = False

    @torch.jit.ignore
    def no_weight_decay(self):
        return {'pos_embed1', 'pos_embed2', 'pos_embed3', 'pos_embed4', 'cls_token'}  # has pos_embed may be better

    def get_classifier(self):
        return self.head

    def reset_classifier(self, num_classes, global_pool=''):
        self.num_classes = num_classes
        self.head = Linear(self.embed_dim, num_classes)

    def forward_features(self, x):
        B = x.shape[0]

        for i in range(self.num_stages):
            patch_embed = getattr(self, f"patch_embed{i + 1}")
            block = getattr(self, f"block{i + 1}")
            norm = getattr(self, f"norm{i + 1}")
            x, H, W = patch_embed(x)
            self.input_resolutions.append([H,W])
            for blk in block:
                x = blk(x, H, W)
            x = norm(x)
            if i != self.num_stages - 1:
                x = x.reshape(B, H, W, -1).permute(0, 3, 1, 2).contiguous()
        self.H_ = x.shape[1]
        return x.mean(dim=1)

    def forward(self, x):
        x = self.forward_features(x)
        x = self.head(x)

        return x

    def reverse_grad(self,grad,input_resolution,window_size):
        H,W = input_resolution
        grad = grad.view(1, H // window_size, W // window_size, window_size, window_size)
        grad = grad.permute(0, 1, 3, 2, 4).contiguous().view(1, H,W)
        return grad

    def relprop(self,cam=None,method="transformer_attribution", is_ablation=False,start_block =None, **kwargs):
        B,C = cam.shape
        cam = self.head.relprop(cam,**kwargs)
    
        cam = cam.unsqueeze(1)
        cam = cam.repeat(1,self.H_,1)

        for i in range(self.num_stages-1,-1,-1):
            patch_embed = getattr(self, f"patch_embed{i + 1}")
            block = getattr(self, f"block{i + 1}")
            norm = getattr(self, f"norm{i + 1}")
            if i != self.num_stages - 1:
                cam = cam.permute(0,2,3,1)
                B,_,_,C = cam.shape
                cam = cam.reshape(B, -1, C)

            cam = norm.relprop(cam,**kwargs)

            # cam = patch_embed.relprop(cam,**kwargs)
            for blk in reversed(block):
                cam = blk.relprop(cam,**kwargs)

            cam = patch_embed.relprop(cam,**kwargs)

        cams = []        
        for i in range(self.num_stages):
            patch_embed = getattr(self, f"patch_embed{i + 1}")
            block = getattr(self, f"block{i + 1}")
            norm = getattr(self, f"norm{i + 1}")
            stage_cam = []
            for blk in block:
                grad = blk.attn.get_attn_gradients()
                cam = blk.attn.get_attn_cam()
                cam = grad * cam
                print(grad.shape)
                cam = cam.clamp(min=0).mean(dim=1)
                stage_cam.append(cam)
            cams.append(stage_cam)

        start_block = -1
        input_resolution = self.input_resolutions[start_block]
        stage_cam = cams[start_block]
        rollout = compute_rollout_attention(stage_cam)
        # cam = rollout[:, 0,:]
        cam = rollout.mean(-1)
        cam = self.reverse_grad(cam,input_resolution,7)
        cam = cam.unsqueeze(1)
        return cam


    def load_from(self,weight):
        if self.mlp_head:
            self_state_dict = self.state_dict()
            state_dict = {k:v for k,v in weight.items() if not k.startswith("head")}
            self_state_dict.update(state_dict)
            self.load_state_dict(self_state_dict)
        else:
            self.load_state_dict(weight)





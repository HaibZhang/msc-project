import torch
import torch.nn as nn
import torch.utils.checkpoint as checkpoint
from explain.util.weight_init import trunc_normal_,pair
from explain.util.explain_layers import *

def compute_rollout_attention(all_layer_matrices, start_layer,end_layer):
    # adding residual consideration
    all_layer_matrices = all_layer_matrices[start_layer:end_layer]
    num_token1,num_token2 = all_layer_matrices[0].shape[1],all_layer_matrices[0].shape[2]
    batch_size = all_layer_matrices[0].shape[0]
    eye = torch.eye(num_token1,num_token2).expand(batch_size, num_token1,num_token2).to(all_layer_matrices[0].device)
    all_layer_matrices = [all_layer_matrices[i] + eye for i in range(len(all_layer_matrices))]
    # all_layer_matrices = [all_layer_matrices[i] / all_layer_matrices[i].sum(dim=-1, keepdim=True)
    #                       for i in range(len(all_layer_matrices))]
    joint_attention = all_layer_matrices[0]
    for i in range(start_layer+1, len(all_layer_matrices)):
        joint_attention = all_layer_matrices[i].bmm(joint_attention)
    return joint_attention


class Mlp(nn.Module):
    def __init__(self, in_features, hidden_features=None, out_features=None, act_layer=nn.GELU, drop=0.):
        super().__init__()
        out_features = out_features or in_features
        hidden_features = hidden_features or in_features
        self.fc1 = Linear(in_features, hidden_features)
        self.act = act_layer()
        self.fc2 = Linear(hidden_features, out_features)
        self.drop = Dropout(drop)

    def forward(self, x):
        x = self.fc1(x)
        x = self.act(x)
        x = self.drop(x)
        x = self.fc2(x)
        x = self.drop(x)
        return x

    def relprop(self, cam, **kwargs):
        cam = self.drop.relprop(cam, **kwargs)
        cam = self.fc2.relprop(cam, **kwargs)
        cam = self.drop.relprop(cam, **kwargs)
        cam = self.act.relprop(cam, **kwargs)
        cam = self.fc1.relprop(cam, **kwargs)
        return cam

def window_partition(x, window_size):
    """
    Args:
        x: (B, H, W, C)
        window_size (int): window size

    Returns:
        windows: (num_windows*B, window_size, window_size, C)
    """
    B, H, W, C = x.shape
    x = x.view(B, H // window_size, window_size, W // window_size, window_size, C)
    windows = x.permute(0, 1, 3, 2, 4, 5).contiguous().view(-1, window_size, window_size, C)
    return windows


def window_reverse(windows, window_size, H, W):
    """
    Args:
        windows: (num_windows*B, window_size, window_size, C)
        window_size (int): Window size
        H (int): Height of image
        W (int): Width of image

    Returns:
        x: (B, H, W, C)
    """
    B = int(windows.shape[0] / (H * W / window_size / window_size))
    x = windows.view(B, H // window_size, W // window_size, window_size, window_size, -1)
    x = x.permute(0, 1, 3, 2, 4, 5).contiguous().view(B, H, W, -1)
    return x


class WindowAttention(nn.Module):
    r""" Window based multi-head self attention (W-MSA) module with relative position bias.
    It supports both of shifted and non-shifted window.

    Args:
        dim (int): Number of input channels.
        window_size (tuple[int]): The height and width of the window.
        num_heads (int): Number of attention heads.
        qkv_bias (bool, optional):  If True, add a learnable bias to query, key, value. Default: True
        qk_scale (float | None, optional): Override default qk scale of head_dim ** -0.5 if set
        attn_drop (float, optional): Dropout ratio of attention weight. Default: 0.0
        proj_drop (float, optional): Dropout ratio of output. Default: 0.0
    """

    def __init__(self, dim, window_size, num_heads, qkv_bias=True, qk_scale=None, attn_drop=0., proj_drop=0.):

        super().__init__()
        self.dim = dim
        self.window_size = window_size  # Wh, Ww
        self.num_heads = num_heads
        head_dim = dim // num_heads
        self.scale = qk_scale or head_dim ** -0.5
        # self.clone = Clone()
        # self.query = Linear(dim, dim, bias=qkv_bias)
        # self.key = Linear(dim, dim, bias=qkv_bias)
        # self.value = Linear(dim, dim, bias=qkv_bias)

        # define a parameter table of relative position bias
        self.relative_position_bias_table = nn.Parameter(torch.zeros((2 * window_size[0] - 1) * (2 * window_size[1] - 1), num_heads))  # 2*Wh-1 * 2*Ww-1, nH

        # get pair-wise relative position index for each token inside the window
        coords_h = torch.arange(self.window_size[0])
        coords_w = torch.arange(self.window_size[1])
        coords = torch.stack(torch.meshgrid([coords_h, coords_w]))  # 2, Wh, Ww
        coords_flatten = torch.flatten(coords, 1)  # 2, Wh*Ww
        relative_coords = coords_flatten[:, :, None] - coords_flatten[:, None, :]  # 2, Wh*Ww, Wh*Ww
        relative_coords = relative_coords.permute(1, 2, 0).contiguous()  # Wh*Ww, Wh*Ww, 2
        relative_coords[:, :, 0] += self.window_size[0] - 1  # shift to start from 0
        relative_coords[:, :, 1] += self.window_size[1] - 1
        relative_coords[:, :, 0] *= 2 * self.window_size[1] - 1
        relative_position_index = relative_coords.sum(-1)  # Wh*Ww, Wh*Ww
        self.register_buffer("relative_position_index", relative_position_index)

        self.attn_drop = Dropout(attn_drop)
        # A = Q*K^T
        self.matmul1 = MatMul()
        self.add1 = Add()
        self.add2 = Add()

        self.qkv = Linear(dim, dim * 3, bias=qkv_bias)

        # attn = A*V
        self.matmul2 = MatMul()
        self.proj = Linear(dim, dim)
        self.proj_drop = Dropout(proj_drop)

        trunc_normal_(self.relative_position_bias_table, std=.02)
        self.softmax = Softmax(dim=-1)

    def get_attn(self):
        return self.attn

    def save_attn(self, attn):
        self.attn = attn

    def save_attn_cam(self, cam):
        self.attn_cam = cam

    def get_attn_cam(self):
        return self.attn_cam

    def get_v(self):
        return self.v

    def save_v(self, v):
        self.v = v

    def save_v_cam(self, cam):
        self.v_cam = cam

    def get_v_cam(self):
        return self.v_cam

    def save_attn_gradients(self, attn_gradients):
        self.attn_gradients = attn_gradients

    def get_attn_gradients(self):
        return self.attn_gradients

    def forward(self, x, mask=None):
        """
        Args:
            x: input features with shape of (num_windows*B, N, C)
            mask: (0/-inf) mask with shape of (num_windows, Wh*Ww, Wh*Ww) or None
        """
        self.B_, self.N, C = x.shape
        self.mask = mask
        # h1, h2, h3 = self.clone(x, 3)
        # q = self.query(h1).reshape(self.B_, self.N, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)
        # k = self.key(h2).reshape(self.B_, self.N, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)
        # v = self.value(h3).reshape(self.B_, self.N, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)

        qkv = self.qkv(x).reshape(self.B_, self.N, 3, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)
        q, k, v = qkv[0], qkv[1], qkv[2]  # make torchscript happy (cannot use tensor as tuple)
        self.save_v(v)

        q = q * self.scale
        attn = self.matmul1((q,k.transpose(-2, -1)))

        relative_position_bias = self.relative_position_bias_table[self.relative_position_index.view(-1)].view(
            self.window_size[0] * self.window_size[1], self.window_size[0] * self.window_size[1], -1)  # Wh*Ww,Wh*Ww,nH
        relative_position_bias = relative_position_bias.permute(2, 0, 1).contiguous()  # nH, Wh*Ww, Wh*Ww
        attn = self.add1((attn , relative_position_bias.unsqueeze(0)))

        if mask is not None:
            nW = mask.shape[0]
            attn = self.add2((attn.view(self.B_ // nW, nW, self.num_heads, self.N, self.N), mask.unsqueeze(1).unsqueeze(0)))
            attn = attn.view(-1, self.num_heads, self.N, self.N)
            attn = self.softmax(attn)
        else:
            attn = self.softmax(attn)

        self.attn = self.attn_drop(attn)
        self.save_attn(attn)
        attn.register_hook(self.save_attn_gradients)

        # x = (attn @ v).transpose(1, 2).reshape(B_, N, C)
        x = self.matmul2((self.attn,v)).transpose(1, 2).reshape(self.B_, self.N, C)
        x = self.proj(x)
        x = self.proj_drop(x)
        return x

    def extra_repr(self) -> str:
        return f'dim={self.dim}, window_size={self.window_size}, num_heads={self.num_heads}'

    def flops(self, N):
        # calculate flops for 1 window with token length of N
        flops = 0
        # qkv = self.qkv(x)
        flops += N * self.dim * 3 * self.dim
        # attn = (q @ k.transpose(-2, -1))
        flops += self.num_heads * N * (self.dim // self.num_heads) * N
        #  x = (attn @ v)
        flops += self.num_heads * N * N * (self.dim // self.num_heads)
        # x = self.proj(x)
        flops += N * self.dim * self.dim
        return flops
    
    def flip_transpose_for_scores(self,x):
        x = x.permute(0,2,1,3)
        new_x_shape = x.size()[:-2] + (self.all_head_size,)
        x = x.view(*new_x_shape)
        return x

    def relprop(self, cam, **kwargs):
        cam = self.proj_drop.relprop(cam, **kwargs)
        cam = self.proj.relprop(cam, **kwargs)
        new_cam_shape = cam.size()[:-1] + (self.num_heads,-1)
        cam = cam.view(*new_cam_shape)
        cam = cam.permute(0, 2, 1, 3).contiguous()

        # attn = A*V
        (cam1, cam_v)= self.matmul2.relprop(cam, **kwargs)
        cam1 /= 2
        cam_v /= 2

        self.save_attn_cam(cam1)
        self.save_v_cam(cam_v)

        cam1 = self.attn_drop.relprop(cam1, **kwargs)
        cam1 = self.softmax.relprop(cam1, **kwargs)

        if self.mask is not None:
            nW = self.mask.shape[0]
            cam1 = cam1.view(self.B_ // nW, nW, self.num_heads, self.N, self.N)
            # [attention_scores, attention_mask]
            (cam1, _) = self.add2.relprop(cam1, **kwargs)
            cam1 = cam1.view(-1, self.num_heads, self.N, self.N)
        
        (cam1, _) = self.add1.relprop(cam1, **kwargs)
        # A = Q*K^T
        (cam_q, cam_k) = self.matmul1.relprop(cam1, **kwargs)
        cam_q /= 2
        cam_k /= 2
        # cam_q = self.flip_transpose_for_scores(cam_q)
        # cam_k = self.flip_transpose_for_scores(cam_k.transpose(-1,-2))
        # cam_v = self.flip_transpose_for_scores(cam_v)

        # cam_q = self.query.relprop(cam_q, **kwargs)
        # cam_k = self.key.relprop(cam_k, **kwargs)
        # cam_v = self.value.relprop(cam_v, **kwargs)
        # cam = self.clone.relprop((cam_q, cam_k, cam_v), **kwargs)

        cam_k = cam_k.transpose(-2,-1)
        cam_k = cam_k.unsqueeze(0)
        cam_q = cam_q.unsqueeze(0)
        cam_v = cam_v.unsqueeze(0)
        cam = torch.cat([cam_q,cam_k,cam_v],dim=0)
        cam = cam.permute(1,3,0,2, 4).contiguous().reshape(self.B_, self.N,-1)
        cam = self.qkv.relprop(cam,**kwargs)

        return cam



class SwinTransformerBlock(nn.Module):
    r""" Swin Transformer Block.

    Args:
        dim (int): Number of input channels.
        input_resolution (tuple[int]): Input resulotion.
        num_heads (int): Number of attention heads.
        window_size (int): Window size.
        shift_size (int): Shift size for SW-MSA.
        mlp_ratio (float): Ratio of mlp hidden dim to embedding dim.
        qkv_bias (bool, optional): If True, add a learnable bias to query, key, value. Default: True
        qk_scale (float | None, optional): Override default qk scale of head_dim ** -0.5 if set.
        drop (float, optional): Dropout rate. Default: 0.0
        attn_drop (float, optional): Attention dropout rate. Default: 0.0
        drop_path (float, optional): Stochastic depth rate. Default: 0.0
        act_layer (nn.Module, optional): Activation layer. Default: nn.GELU
        norm_layer (nn.Module, optional): Normalization layer.  Default: nn.LayerNorm
    """

    def __init__(self, dim, input_resolution, num_heads, window_size=7, shift_size=0,
                 mlp_ratio=4., qkv_bias=True, qk_scale=None, drop=0., attn_drop=0., drop_path=0.,
                 act_layer=GELU, norm_layer=LayerNorm):
        super().__init__()
        self.dim = dim
        self.input_resolution = input_resolution
        self.num_heads = num_heads
        self.window_size = window_size
        self.shift_size = shift_size
        self.mlp_ratio = mlp_ratio
        if min(self.input_resolution) <= self.window_size:
            # if window size is larger than input resolution, we don't partition windows
            self.shift_size = 0
            self.window_size = min(self.input_resolution)
        assert 0 <= self.shift_size < self.window_size, "shift_size must in 0-window_size"

        self.norm1 = norm_layer(dim)
        self.attn = WindowAttention(
            dim, window_size=pair(self.window_size), num_heads=num_heads,
            qkv_bias=qkv_bias, qk_scale=qk_scale, attn_drop=attn_drop, proj_drop=drop)

        self.drop_path1 = DropPath(drop_path) if drop_path > 0. else Identity()
        self.drop_path2 = DropPath(drop_path) if drop_path > 0. else Identity()
        self.norm2 = norm_layer(dim)
        mlp_hidden_dim = int(dim * mlp_ratio)
        self.mlp = Mlp(in_features=dim, hidden_features=mlp_hidden_dim, act_layer=act_layer, drop=drop)

        if self.shift_size > 0:
            # calculate attention mask for SW-MSA
            H, W = self.input_resolution
            img_mask = torch.zeros((1, H, W, 1))  # 1 H W 1
            h_slices = (slice(0, -self.window_size),
                        slice(-self.window_size, -self.shift_size),
                        slice(-self.shift_size, None))
            w_slices = (slice(0, -self.window_size),
                        slice(-self.window_size, -self.shift_size),
                        slice(-self.shift_size, None))
            cnt = 0
            for h in h_slices:
                for w in w_slices:
                    img_mask[:, h, w, :] = cnt
                    cnt += 1

            mask_windows = window_partition(img_mask, self.window_size)  # nW, window_size, window_size, 1
            mask_windows = mask_windows.view(-1, self.window_size * self.window_size)
            attn_mask = mask_windows.unsqueeze(1) - mask_windows.unsqueeze(2)
            attn_mask = attn_mask.masked_fill(attn_mask != 0, float(-100.0)).masked_fill(attn_mask == 0, float(0.0))
        else:
            attn_mask = None

        self.register_buffer("attn_mask", attn_mask)
        self.add1 = Add()
        self.add2 = Add()
        self.clone1 = Clone()
        self.clone2 = Clone()

    def get_attn(self):
        return self.attention

    def save_attn(self, attn):
        self.attention = attn

    def save_attn_cam(self, cam):
        self.attn_cam = cam

    def get_attn_cam(self):
        return self.attn_cam

    def get_v(self):
        return self.v

    def save_v(self, v):
        self.v = v

    def save_v_cam(self, cam):
        self.v_cam = cam

    def get_v_cam(self):
        return self.v_cam

    def save_attn_gradients(self, attn_gradients):
        self.attn_gradients = attn_gradients

    def get_attn_gradients(self):
        return self.attn_gradients

    def forward(self, x):
        H, W = self.input_resolution
        self.B, L, self.C = x.shape
        assert L == H * W, "input feature has wrong size"

        shortcut,x = self.clone1(x,2)
        x = self.norm1(x)
        x = x.view(self.B, H, W, self.C)

        # cyclic shift
        if self.shift_size > 0:
            shifted_x = torch.roll(x, shifts=(-self.shift_size, -self.shift_size), dims=(1, 2))
        else:
            shifted_x = x

        # partition windows
        x_windows = window_partition(shifted_x, self.window_size)  # nW*B, window_size, window_size, C
        x_windows = x_windows.view(-1, self.window_size * self.window_size, self.C)  # nW*B, window_size*window_size, C

        # W-MSA/SW-MSA
        attn_windows = self.attn(x_windows, mask=self.attn_mask)  # nW*B, window_size*window_size, C

        self.save_v(self.attn.get_v())
        self.save_attn(self.attn.get_attn())
        self.attn.get_attn().register_hook(self.save_attn_gradients)
        
        # merge windows
        attn_windows = attn_windows.view(-1, self.window_size, self.window_size, self.C)
        shifted_x = window_reverse(attn_windows, self.window_size, H, W)  # B H' W' C

        # reverse cyclic shift
        if self.shift_size > 0:
            x = torch.roll(shifted_x, shifts=(self.shift_size, self.shift_size), dims=(1, 2))
        else:
            x = shifted_x

        x = x.view(self.B, H * W, self.C)
        # FFN
        x = self.add1((shortcut, self.drop_path1(x)))
        x1,x2 = self.clone2(x,2)
        x = self.add2((x1,self.drop_path2(self.mlp(self.norm2(x2)))))

        return x

    def extra_repr(self) -> str:
        return f"dim={self.dim}, input_resolution={self.input_resolution}, num_heads={self.num_heads}, " \
               f"window_size={self.window_size}, shift_size={self.shift_size}, mlp_ratio={self.mlp_ratio}"

    def flops(self):
        flops = 0
        H, W = self.input_resolution
        # norm1
        flops += self.dim * H * W
        # W-MSA/SW-MSA
        nW = H * W / self.window_size / self.window_size
        flops += nW * self.attn.flops(self.window_size * self.window_size)
        # mlp
        flops += 2 * H * W * self.dim * self.dim * self.mlp_ratio
        # norm2
        flops += self.dim * H * W
        return flops

    def relprop(self, cam, **kwargs):
        H, W = self.input_resolution
        (cam1,cam2) = self.add2.relprop(cam, **kwargs)
        cam2 =self.drop_path2.relprop(cam2,**kwargs)
        cam2 = self.mlp.relprop(cam2, **kwargs)
        cam2 = self.norm2.relprop(cam2, **kwargs)

        cam1 = self.clone2.relprop((cam1,cam2), **kwargs)

        (cam1,cam2) = self.add1.relprop(cam1, **kwargs)
        cam2 =self.drop_path1.relprop(cam2,**kwargs)
        cam2 = cam2.view(self.B, H , W, self.C)
        if self.shift_size > 0:
            cam2 = torch.roll(cam2, shifts=(-self.shift_size, -self.shift_size), dims=(1, 2))

        cam2 = window_partition(cam2, self.window_size) 
        cam2 = cam2.view(-1, self.window_size*self.window_size, self.C)
        cam2 = self.attn.relprop(cam2, **kwargs) 
        cam2 = window_reverse(cam2, self.window_size, H, W)  # B H' W' C
        if self.shift_size > 0:
            cam2 = torch.roll(cam2, shifts=(self.shift_size, self.shift_size), dims=(1, 2))

        cam2 = cam2.view(self.B, H*W, self.C)
        cam2 = self.norm1.relprop(cam2,**kwargs)
        cam = self.clone1.relprop((cam1,cam2),**kwargs)
        return cam



class PatchMerging(nn.Module):
    r""" Patch Merging Layer.

    Args:
        input_resolution (tuple[int]): Resolution of input feature.
        dim (int): Number of input channels.
        norm_layer (nn.Module, optional): Normalization layer.  Default: nn.LayerNorm
    """

    def __init__(self, input_resolution, dim, norm_layer=LayerNorm):
        super().__init__()
        self.input_resolution = input_resolution
        self.dim = dim
        self.reduction = Linear(4 * dim, 2 * dim, bias=False)
        self.norm = norm_layer(4 * dim)

    def forward(self, x):
        """
        x: B, H*W, C
        """
        H, W = self.input_resolution
        self.B, L, self.C = x.shape
        assert L == H * W, "input feature has wrong size"
        assert H % 2 == 0 and W % 2 == 0, f"x size ({H}*{W}) are not even."

        x = x.view(self.B, H, W, self.C)

        x0 = x[:, 0::2, 0::2, :]  # B H/2 W/2 C
        x1 = x[:, 1::2, 0::2, :]  # B H/2 W/2 C
        x2 = x[:, 0::2, 1::2, :]  # B H/2 W/2 C
        x3 = x[:, 1::2, 1::2, :]  # B H/2 W/2 C
        x = torch.cat([x0, x1, x2, x3], -1)  # B H/2 W/2 4*C
        x = x.view(self.B, -1, 4 * self.C)  # B H/2*W/2 4*C

        x = self.norm(x)
        x = self.reduction(x)

        return x

    def extra_repr(self) -> str:
        return f"input_resolution={self.input_resolution}, dim={self.dim}"

    def flops(self):
        H, W = self.input_resolution
        flops = H * W * self.dim
        flops += (H // 2) * (W // 2) * 4 * self.dim * 2 * self.dim
        return flops



    def relprop(self, cam, **kwargs):
        H, W = self.input_resolution
        cam =self.reduction.relprop(cam,**kwargs)
        cam = self.norm.relprop(cam, **kwargs)
        cam = cam.view(self.B, int(H/2) ,int(W/2), 4 * self.C)  # B H/2*W/2 4*C
        cam1,cam2,cam3,cam4 = torch.chunk(cam,4,-1)

        cam1 = cam1.permute(0,3,2,1).contiguous().unsqueeze(-1)
        cam2 = cam2.permute(0,3,2,1).contiguous().unsqueeze(-1)
        cam12 = torch.cat([cam1,cam2],-1).view(self.B,self.C,int(W/2),-1)

        cam3 = cam3.permute(0,3,2,1).contiguous().unsqueeze(-1)
        cam4 = cam4.permute(0,3,2,1).contiguous().unsqueeze(-1)
        cam34 = torch.cat([cam3,cam4],-1).view(self.B,self.C,int(W/2),-1)

        cam12 = cam12.permute(0,1,3,2).contiguous().unsqueeze(-1)
        cam34 = cam34.permute(0,1,3,2).contiguous().unsqueeze(-1)
        
        cam = torch.cat([cam12,cam34],-1).view(self.B,self.C, H,-1).permute(0,2,3,1).contiguous()
        cam = cam.view(self.B,-1,self.C)
        return cam


class BasicLayer(nn.Module):
    """ A basic Swin Transformer layer for one stage.

    Args:
        dim (int): Number of input channels.
        input_resolution (tuple[int]): Input resolution.
        depth (int): Number of blocks.
        num_heads (int): Number of attention heads.
        window_size (int): Local window size.
        mlp_ratio (float): Ratio of mlp hidden dim to embedding dim.
        qkv_bias (bool, optional): If True, add a learnable bias to query, key, value. Default: True
        qk_scale (float | None, optional): Override default qk scale of head_dim ** -0.5 if set.
        drop (float, optional): Dropout rate. Default: 0.0
        attn_drop (float, optional): Attention dropout rate. Default: 0.0
        drop_path (float | tuple[float], optional): Stochastic depth rate. Default: 0.0
        norm_layer (nn.Module, optional): Normalization layer. Default: nn.LayerNorm
        downsample (nn.Module | None, optional): Downsample layer at the end of the layer. Default: None
        use_checkpoint (bool): Whether to use checkpointing to save memory. Default: False.
    """

    def __init__(self, dim, input_resolution, depth, num_heads, window_size,
                 mlp_ratio=4., qkv_bias=True, qk_scale=None, drop=0., attn_drop=0.,
                 drop_path=0., norm_layer=LayerNorm, downsample=None, use_checkpoint=False):

        super().__init__()
        self.dim = dim
        self.input_resolution = input_resolution
        self.depth = depth
        self.use_checkpoint = use_checkpoint

        # build blocks
        self.blocks = nn.ModuleList([
            SwinTransformerBlock(dim=dim, input_resolution=input_resolution,
                                 num_heads=num_heads, window_size=window_size,
                                 shift_size=0 if (i % 2 == 0) else window_size // 2,
                                 mlp_ratio=mlp_ratio,
                                 qkv_bias=qkv_bias, qk_scale=qk_scale,
                                 drop=drop, attn_drop=attn_drop,
                                 drop_path=drop_path[i] if isinstance(drop_path, list) else drop_path,
                                 norm_layer=norm_layer)
            for i in range(depth)])

        # patch merging layer
        if downsample is not None:
            self.downsample = downsample(input_resolution, dim=dim, norm_layer=norm_layer)
        else:
            self.downsample = None

    def forward(self, x):
        for blk in self.blocks:
            if self.use_checkpoint:
                x = checkpoint.checkpoint(blk, x)
            else:
                x = blk(x)
        if self.downsample is not None:
            x = self.downsample(x)
        return x

    def extra_repr(self) -> str:
        return f"dim={self.dim}, input_resolution={self.input_resolution}, depth={self.depth}"

    def flops(self):
        flops = 0
        for blk in self.blocks:
            flops += blk.flops()
        if self.downsample is not None:
            flops += self.downsample.flops()
        return flops


    def relprop(self, cam, **kwargs):

        if self.downsample is not None:
            cam = self.downsample.relprop(cam, **kwargs)
        for blk in reversed(self.blocks):
            cam = blk.relprop(cam, **kwargs)

        return cam


class PatchEmbed(nn.Module):
    r""" Image to Patch Embedding

    Args:
        img_size (int): Image size.  Default: 224.
        patch_size (int): Patch token size. Default: 4.
        in_chans (int): Number of input image channels. Default: 3.
        embed_dim (int): Number of linear projection output channels. Default: 96.
        norm_layer (nn.Module, optional): Normalization layer. Default: None
    """

    def __init__(self, img_size=224, patch_size=4, in_chans=3, embed_dim=96, norm_layer=None):
        super().__init__()
        img_size = pair(img_size)
        patch_size = pair(patch_size)
        patches_resolution = [img_size[0] // patch_size[0], img_size[1] // patch_size[1]]
        self.img_size = img_size
        self.patch_size = patch_size
        self.patches_resolution = patches_resolution
        self.num_patches = patches_resolution[0] * patches_resolution[1]

        self.in_chans = in_chans
        self.embed_dim = embed_dim

        self.proj = Conv2d(in_chans, embed_dim, kernel_size=patch_size, stride=patch_size)
        if norm_layer is not None:
            self.norm = norm_layer(embed_dim)
        else:
            self.norm = None

    def forward(self, x):
        B, C, self.H, self.W = x.shape
        # FIXME look at relaxing size constraints
        assert self.H == self.img_size[0] and self.W == self.img_size[1], \
            f"Input image size ({H}*{W}) doesn't match model ({self.img_size[0]}*{self.img_size[1]})."
        x = self.proj(x)
        self.B,self.pc,self.ph,self.pw = x.shape
        x = x.flatten(2).transpose(1, 2)  # B Ph*Pw C
        if self.norm is not None:
            x = self.norm(x)
        return x

    def flops(self):
        Ho, Wo = self.patches_resolution
        flops = Ho * Wo * self.embed_dim * self.in_chans * (self.patch_size[0] * self.patch_size[1])
        if self.norm is not None:
            flops += Ho * Wo * self.embed_dim
        return flops


    def relprop(self, cam, **kwargs):
        if self.norm is not None:
            cam = self.norm.relprop(cam,**kwargs)
        cam = cam.transpose(1,2)
        cam = cam.reshape(cam.shape[0], cam.shape[1],
                     (self.img_size[0] // self.patch_size[0]), (self.img_size[1] // self.patch_size[1]))
        cam = self.proj.relprop(cam,**kwargs)
        return cam

        

class SWIN(nn.Module):
    r""" Swin Transformer
        A PyTorch impl of : `Swin Transformer: Hierarchical Vision Transformer using Shifted Windows`  -
          https://arxiv.org/pdf/2103.14030

    Args:
        img_size (int | tuple(int)): Input image size. Default 224
        patch_size (int | tuple(int)): Patch size. Default: 4
        in_chans (int): Number of input image channels. Default: 3
        num_classes (int): Number of classes for classification head. Default: 1000
        embed_dim (int): Patch embedding dimension. Default: 96
        depths (tuple(int)): Depth of each Swin Transformer layer.
        num_heads (tuple(int)): Number of attention heads in different layers.
        window_size (int): Window size. Default: 7
        mlp_ratio (float): Ratio of mlp hidden dim to embedding dim. Default: 4
        qkv_bias (bool): If True, add a learnable bias to query, key, value. Default: True
        qk_scale (float): Override default qk scale of head_dim ** -0.5 if set. Default: None
        drop_rate (float): Dropout rate. Default: 0
        attn_drop_rate (float): Attention dropout rate. Default: 0
        drop_path_rate (float): Stochastic depth rate. Default: 0.1
        norm_layer (nn.Module): Normalization layer. Default: nn.LayerNorm.
        ape (bool): If True, add absolute position embedding to the patch embedding. Default: False
        patch_norm (bool): If True, add normalization after patch embedding. Default: True
        use_checkpoint (bool): Whether to use checkpointing to save memory. Default: False
    """

    def __init__(self,config, img_size=224,  in_chans=3, num_classes=1000,

                 use_checkpoint=False, **kwargs):
        super().__init__()
        patch_size = config.patch_size
        embed_dim = config.embed_dim
        self.depths=config.depths
        self.num_heads = config.num_heads
        self.window_size=config.window_size
        mlp_ratio=config.mlp_ratio
        qkv_bias=config.qkv_bias
        qk_scale=config.qk_scale
        drop_rate=config.drop_rate 
        attn_drop_rate=config.drop_rate
        drop_path_rate=config.drop_path_rate
        norm_layer=LayerNorm
        ape=config.ape
        patch_norm=config.patch_norm,

        self.num_classes = num_classes
        self.num_layers = len(self.depths)
        self.embed_dim = embed_dim
        self.ape = ape
        self.patch_norm = patch_norm
        self.num_features = int(embed_dim * 2 ** (self.num_layers - 1))
        self.mlp_ratio = mlp_ratio
        self.mlp_head = config.mlp_head
        # split image into non-overlapping patches
        self.patch_embed = PatchEmbed(
            img_size=img_size, patch_size=patch_size, in_chans=in_chans, embed_dim=embed_dim,
            norm_layer=norm_layer if self.patch_norm else None)
        num_patches = self.patch_embed.num_patches
        patches_resolution = self.patch_embed.patches_resolution
        self.patches_resolution = patches_resolution

        # absolute position embedding
        if self.ape:
            self.absolute_pos_embed = nn.Parameter(torch.zeros(1, num_patches, embed_dim))
            trunc_normal_(self.absolute_pos_embed, std=.02)

        self.pos_drop = Dropout(p=drop_rate)

        # stochastic depth
        dpr = [x.item() for x in torch.linspace(0, drop_path_rate, sum(self.depths))]  # stochastic depth decay rule

        # build layers
        self.input_resolutions = []
        self.layers = nn.ModuleList()
        for i_layer in range(self.num_layers):
            input_resolution = (patches_resolution[0] // (2 ** i_layer),patches_resolution[1] // (2 ** i_layer))
            self.input_resolutions.append(input_resolution)
            layer = BasicLayer(dim=int(embed_dim * 2 ** i_layer),
                               input_resolution=input_resolution,
                               depth=self.depths[i_layer],
                               num_heads=self.num_heads[i_layer],
                               window_size=self.window_size,
                               mlp_ratio=self.mlp_ratio,
                               qkv_bias=qkv_bias, qk_scale=qk_scale,
                               drop=drop_rate, attn_drop=attn_drop_rate,
                               drop_path=dpr[sum(self.depths[:i_layer]):sum(self.depths[:i_layer + 1])],
                               norm_layer=norm_layer,
                               downsample=PatchMerging if (i_layer < self.num_layers - 1) else None,
                               use_checkpoint=use_checkpoint)
            self.layers.append(layer)

        self.norm = norm_layer(self.num_features)
        self.avgpool = AdaptiveAvgPool1d(1)
        if self.mlp_head:
            self.head = Mlp(in_features=self.num_features, hidden_features=int(self.num_features * self.mlp_ratio),out_features=num_classes)
        else:
            self.head = Linear(self.num_features, num_classes) if num_classes > 0 else Identity()

        self.add = Add()
        self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, Linear):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, Linear) and m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif isinstance(m, LayerNorm):
            nn.init.constant_(m.bias, 0)
            nn.init.constant_(m.weight, 1.0)

    @torch.jit.ignore
    def no_weight_decay(self):
        return {'absolute_pos_embed'}

    @torch.jit.ignore
    def no_weight_decay_keywords(self):
        return {'relative_position_bias_table'}

    def forward_features(self, x):
        x = self.patch_embed(x)
        if self.ape:
            x = self.add((x,self.absolute_pos_embed))
        x = self.pos_drop(x)

        for layer in self.layers:
            x = layer(x)

        x = self.norm(x)  # B L C
        x = self.avgpool(x.transpose(1, 2))  # B C 1
        x = torch.flatten(x, 1)
        return x

    def forward(self, x):
        x = self.forward_features(x)
        x = self.head(x)
        return x
    
    def reverse_grad(self,grad,input_resolution,window_size):
        H,W = input_resolution
        grad = grad.view(1, H // window_size, W // window_size, window_size, window_size)
        grad = grad.permute(0, 1, 3, 2, 4).contiguous().view(1, H,W)
        return grad
    
    def relprop(self, cam=None,method="transformer_attribution", is_ablation=False, start_block=0, **kwargs):
        cam = self.head.relprop(cam, **kwargs)
        cam = cam.unsqueeze(-1)
        cam = self.avgpool.relprop(cam, **kwargs)
        cam = cam.transpose(1, 2)
        cam = self.norm.relprop(cam,**kwargs)
        for layer in reversed(self.layers):
            cam = layer.relprop(cam,**kwargs)
        cam = self.pos_drop.relprop(cam,**kwargs)
        if self.ape:
            cam,_ = self.add.relprop(cam,**kwargs)

        cam = self.patch_embed.relprop(cam,**kwargs)
        if method == "transformer_attribution":
            cams = []
            for layer in self.layers:
                for blk in layer.blocks:
                    grad = blk.attn.get_attn_gradients()
                    cam = blk.attn.get_attn_cam()
                    cam = grad * cam
                    print(cam.shape)
                    cam = cam.clamp(min=0).mean(dim=1)
                    cams.append(cam)

            start_block = 3
            input_resolution = self.input_resolutions[start_block]
            start_layer=sum(self.depths[:start_block])
            end_layer = start_layer+self.depths[start_block]
            rollout = compute_rollout_attention(cams,  start_layer=start_layer,end_layer=end_layer)
            # cam = rollout[:, 0,:]
            cam = rollout.mean(1)
            cam = self.reverse_grad(cam,input_resolution,self.window_size)
            cam = cam.unsqueeze(1)
            return cam

        elif method == "rollout":
            # cam rollout
            attn_cams = []
            for layer in self.layers:
                for blk in layer.blocks:
                    attn_heads = blk.attn.get_attn_cam().clamp(min=0)
                    avg_heads = (attn_heads.sum(dim=1) / attn_heads.shape[1]).detach()
                    attn_cams.append(avg_heads)
            start_block = 3
            input_resolution = self.input_resolutions[start_block]
            start_layer=sum(self.depths[:start_block])
            end_layer = start_layer+self.depths[start_block]
            rollout = compute_rollout_attention(attn_cams,  start_layer=start_layer,end_layer=end_layer)
            cam = rollout.mean(1)
            cam = self.reverse_grad(cam,input_resolution,self.window_size)
            cam = cam.unsqueeze(1)
            # attn_cams = attn_cams[:, 0, 1:]
            return cam

    def flops(self):
        flops = 0
        flops += self.patch_embed.flops()
        for i, layer in enumerate(self.layers):
            flops += layer.flops()
        flops += self.num_features * self.patches_resolution[0] * self.patches_resolution[1] // (2 ** self.num_layers)
        flops += self.num_features * self.num_classes
        return flops

    def load_from(self,weight):
        weight = weight['model']
        if self.mlp_head:
            nn.init.zeros_(self.head.fc1.weight)
            nn.init.zeros_(self.head.fc1.bias)
            nn.init.zeros_(self.head.fc2.weight)
            nn.init.zeros_(self.head.fc2.bias)
            self_state_dict = self.state_dict()
            state_dict = {k:v for k,v in weight.items() if not k.startswith("head")}
            self_state_dict.update(state_dict)
            self.load_state_dict(self_state_dict)
        else:
            self.load_state_dict(weight)





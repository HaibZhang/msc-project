# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import ml_collections

def vit_b16_config():
    """Returns the ViT-B/16 configuration."""
    config = ml_collections.ConfigDict()
    config.patches = ml_collections.ConfigDict({'size': (16, 16)})
    config.hidden_size = 768
    config.transformer = ml_collections.ConfigDict()
    config.transformer.mlp_ratio = 4
    config.transformer.num_heads = 12
    config.transformer.num_layers = 12
    config.transformer.attention_dropout_rate = 0.0
    config.transformer.dropout_rate = 0.1
    config.classifier = 'token'
    config.representation_size = None
    config.mlp_head = False
    return config


def swin_b16_config():
    """Returns the swin_b16 configuration."""
    config = ml_collections.ConfigDict()
    config.patch_size = 4
    config.embed_dim = 128
    config.depths = [ 2, 2, 18, 2 ]
    # config.num_heads = [3, 6, 12, 24]
    config.num_heads = [4, 8, 16, 32]
    config.window_size = 7
    config.hidden_size = 768
    config.mlp_ratio = 4
    config.qkv_bias = True
    config.ape = False
    config.patch_norm = True
    config.qk_scale = None
    config.drop_path_rate = 0.5
    config.drop_rate = 0.0
    config.classifier = 'token'
    config.attn_drop_rate = 0.
    config.mlp_head = False
    return config

def pvt_b1_config():
    """Returns the pvt_b1 configuration."""
    config = ml_collections.ConfigDict()
    config.patch_size = 4
    config.embed_dims = [64, 128, 320, 512]
    config.num_heads = [1, 2, 5, 8]
    config.mlp_ratios = [8, 8, 4, 4]
    config.qkv_bias = True
    config.depths=[2, 2, 2, 2]
    config.sr_ratios=[8, 4, 2, 1]
    config.input_size = (3, 224, 224)
    config.pool_size = None
    config.crop_pct = .9
    config.interpolation = "bicubic"
    config.fixed_input_size = True
    config.mean = (0.5, 0.5, 0.5)
    config.std = (0.5, 0.5, 0.5)
    config.first_conv = 'patch_embed.proj'
    config.classifier = 'head'
    config.mlp_head = False
    config.drop_path = 0.1
    config.clip_grad = None
    config.num_stages = 4
    config.in_chans = 3
    config.qk_scale = None
    config.drop_rate = 0.0
    config.attn_drop_rate = 0
    config.drop_path_rate = 0
    config.linear = False
    return config

def pvt_b1_linear_config():
    """Returns the pvt_b1_linear configuration."""
    config = ml_collections.ConfigDict()
    config.patch_size = 4
    config.embed_dims = [64, 128, 320, 512]
    config.num_heads = [1, 2, 5, 8]
    config.mlp_ratios = [8, 8, 4, 4]
    config.qkv_bias = True
    config.depths=[3, 4, 6, 3]
    config.sr_ratios=[8, 4, 2, 1]
    config.input_size = (3, 224, 224)
    config.pool_size = None
    config.crop_pct = .9
    config.interpolation = "bicubic"
    config.fixed_input_size = True
    config.mean = (0.5, 0.5, 0.5)
    config.std = (0.5, 0.5, 0.5)
    config.first_conv = 'patch_embed.proj'
    config.classifier = 'head'
    config.mlp_head = False
    config.drop_path = 0.1
    config.clip_grad = None
    config.num_stages = 4
    config.in_chans = 3
    config.qk_scale = None
    config.drop_rate = 0.0
    config.attn_drop_rate = 0
    config.drop_path_rate = 0
    config.linear = True
    return config


CONFIGS = {
    'ViT-B_16': vit_b16_config(),
    "Swin-B_16": swin_b16_config(),
    "pvt_b1": pvt_b1_config(),
    "pvt_b1_linear":pvt_b1_linear_config()
    }
